import { UPDATE_LABORATORIOS } from '../Actions/LaboratoriosActions';

export default function laboratoriosReducer(state = '', { type, payload }){
    switch(type){
        case UPDATE_LABORATORIOS:
            return payload.laboratorios;
        default:
            return state;
    }
}