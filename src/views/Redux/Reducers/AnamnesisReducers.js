import { UPDATE_ANAMNESIS } from '../Actions/AnamnesisActions';

export default function anamnesisReducer(state = '', { type, payload }){
    switch(type){
        case UPDATE_ANAMNESIS:
            return payload.anamnesis;
        default:
            return state;
    }
}