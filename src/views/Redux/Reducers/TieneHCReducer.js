import { UPDATE_HC } from '../Actions/TieneHCActions';

export default function hcReducer(state = '', { type, payload }){
    switch(type){
        case UPDATE_HC:
            return payload.hc;
        default:
            return state;
    }
}