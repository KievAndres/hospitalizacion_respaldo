import { UPDATE_ANTECEDENTESPNP } from '../Actions/AntecedentesPNPActions';

export default function antecedentespnpReducer(state = '', { type, payload }){
    switch(type){
        case UPDATE_ANTECEDENTESPNP:
            return payload.antecedentespnp;
        default:
            return state;
    }
}