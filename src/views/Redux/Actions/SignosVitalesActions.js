export const UPDATE_SIGNOSVITALES = 'users:updateSignosVitales';

export function updateSignosVitales(newSignosVitales){
    return{
        type: UPDATE_SIGNOSVITALES,
        payload: {
            signosvitales: newSignosVitales
        }
    }
}