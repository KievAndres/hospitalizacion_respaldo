export const UPDATE_ANTECEDENTESFAM = 'antecedentesfams:updateAntecedentesFam';

export function updateAntecedentesFam(newAntecedentesFam){
    return{
        type: UPDATE_ANTECEDENTESFAM,
        payload: {
            antecedentesfam: newAntecedentesFam
        }
    }
}