export const UPDATE_LABORATORIOS = 'laboratorioss:updateLaboratorios';

export function updateLaboratorios(newLaboratorios){
    return{
        type: UPDATE_LABORATORIOS,
        payload: {
            laboratorios: newLaboratorios
        }
    }
}