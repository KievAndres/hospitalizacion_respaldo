import React, { Component } from 'react';
import {
    Badge,
    Button,
    ButtonDropdown,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    Col,
    Collapse,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Fade,
    Form,
    FormGroup,
    FormText,
    FormFeedback,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Label,
    ListGroup,
    ListGroupItem,
    Row,
    Table,
  } from 'reactstrap';
import Encabezado from '../Encabezado/Encabezado';
import EncabezadoDatos from '../EncabezadoDatos/EncabezadoDatos';
import ListGroups from '../Base/ListGroups/ListGroups';

import { connect } from 'react-redux';

import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';
import renderHTML from 'react-render-html';

class NotaInternacionE extends Component {
    constructor(props){
        super(props)

        var today = new Date(),
            date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();

            this.state = {
                fecha: date,
                hora: new Date(),
    
                // =========================
                historiaclinicaver: false,
                notadeinternacionver: false,
                hojadereferenciaver: false,
                hojaevolucion: false,
                menuver: true,

                plan: '',
                indicaciones: [{indicacion: ''}],
                examenfisicodinamico: [],
                examenfisicodinamicoR: [{examenf: ''}],
                fechaprogramada: '',
            }
            this.handleChangePlan = this.handleChangePlan.bind(this);
    }

    async componentDidMount() {
            let newExamenFisico = [
                {
                    parte: 'Estado general',
                    descripcion: this.props.examenf.estado_general
                },
                {
                    parte: 'Piel y faneras',
                    descripcion: this.props.examenf.piel_faneras
                },
                {
                    parte: 'Cara y cuello',
                    descripcion: this.props.examenf.cara_cuello
                },
                {
                    parte: 'Visual y auditivo',
                    descripcion: this.props.examenf.visual_auditivo
                },
                {
                    parte: 'Tiroides',
                    descripcion: this.props.examenf.tiroides
                },
                {
                    parte: 'Cardiopulmonar',
                    descripcion: this.props.examenf.cardiopulmonar
                },
                {
                    parte: 'Abdomen',
                    descripcion: this.props.examenf.abdomen
                },
                {
                    parte: 'Genitales',
                    descripcion: this.props.examenf.genitales
                },
                {
                    parte: 'Extremidades',
                    descripcion: this.props.examenf.extremidades
                },
                {
                    parte: 'Neurologico',
                    descripcion: this.props.examenf.neurologico
                }
            ]
            //Guardando los datos del afiliado
            this.setState({
                examenfisicodinamico: newExamenFisico,
                examenfisicodinamicoR: newExamenFisico
            });
    }

    handleChangePlan(value){
        this.setState({plan: value})
    }

    adicionaIndicacion = () => {
        this.setState({
            indicaciones: this.state.indicaciones.concat([{indicacion: ''}]),
        })
    }
    adicionaIndicacionSetState = (idx) => (evt) => {
        const newIndicacion = this.state.indicaciones.map((item, sidx) => {
            if(idx !== sidx) return item;
            return {indicacion: evt.target.value}
        });
        this.setState({indicaciones: newIndicacion})
    }
    eliminaIndicacion = (idx) => () => {
        this.setState({
            indicaciones: this.state.indicaciones.filter((s, sidx) => idx !== sidx)
        });
    }

    eliminaExamenF = (idx) => () => {
        this.setState({
            examenfisicodinamico: this.state.examenfisicodinamico.filter((s, sidx) => idx !== sidx)
        })
    }
    reiniciaExamenF = () => {
        this.setState({
            examenfisicodinamico: this.state.examenfisicodinamicoR
        })
    }

    handleHora(h){
        this.setState({
            hora: h
        })
    }

    enviarDatos = () =>{
        // Registro de Internacion
        let urlri=''
        let datari={}
        let id_internacion = 'ri' + this.props.paciente.dip + this.state.fecha
        urlri=process.env.REACT_APP_API_URL+'api/v1/registrointernacion/'
        datari={
            id_internacion,
            tratamiento_quirurgico: this.state.plan,
            motivo_hospitalizacion: this.props.historialclinico.diagnostico_presuntivo,
            fecha_internacion: this.state.fechaprogramada,
            estado_internacion: 'En espera'
        }
        try{
            fetch(urlri, {
                method: 'POST',
                body: JSON.stringify(datari),
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(res => res.json())
            .catch(error => console.error('Error:', error))
            .then(response => console.log('Registro Internacion:', response))
            .then(() => {
                let urlni=''
                let datani={}

                urlni=process.env.REACT_APP_API_URL+'api/v1/notaindicaciones/'
                this.state.indicaciones.map((item) => {
                    datani={
                        fecha_indicacion: this.state.fecha,
                        hora_indicacion: this.state.hora.toLocaleTimeString(),
                        indicacioni: item.indicacion,
                        medico: 1,
                        historia_clinica: this.props.historialclinico.id_historiaclinica,
                        internacion: id_internacion
                    }
                    try{
                        fetch(urlni,{
                            method: 'POST',
                            body: JSON.stringify(datani),
                            headers:{
                                'Content-Type': 'application/json'
                            }
                        }).then(res => res.json())
                        .catch(error => console.error('Error:', error))
                        .then(response => console.log('Noas de Indicaciones:', response));
                    }catch(e){
                        console.log(e);
                    }
                    return 0;
                })
                
            });
        }catch(e){
            console.log(e)
        }
    }

    render() {  
        console.log(this.state.fechaprogramada);
        return (
            <div className="animated fadeIn" class="historiaclinica">
                <Row>
                <Col xs="12" md="12">
                <Card>
                    <CardBody>
                        <Encabezado />
                        <EncabezadoDatos titulo="NOTA DE INTERNACION" />

                        <FormGroup>
                        <div class="prueba-hr">
                            <h4 class="seccion">Antecedentes de importancia</h4>
                            <hr class="hr-dinamico"/>
                        </div>
                        </FormGroup>
                        <FormGroup>
                            <p>
                                {this.props.antecedentesfam.length > 0 ?
                                    <div>
                                        <b>Antecedentes familiares: </b>
                                        <ul>
                                            {this.props.antecedentesfam.map((item) => (
                                                <li key={'af'+item.id}>{item.familiar} con {item.antecedentef}</li>
                                            ))}
                                        </ul>
                                    </div> 
                                    : 
                                    <span><b>Antecedentes familiares: </b> No refiere<br/></span>
                                    
                                    
                                }

                                {this.props.antecedentesc.length > 0 ?
                                    <div>
                                        <b>Antecedentes patológicos: </b>
                                        <ul>
                                            {this.props.antecedentesc.map((item) => (
                                                <li key={'ac'+item.id}>{item.antecedentec}</li>
                                            ))}
                                        </ul>
                                    </div>
                                    : 
                                    <span><b>Antecedentes patológicos: </b> Negados<br/></span>
                                }

                                {this.props.antecedentesq.length > 0 ?
                                    <div>
                                        <b>Antecedentes quirúrgicos: </b>
                                        <ul>
                                            {this.props.antecedentesq.map((item) => (
                                                <li key={'aq'+item.id}>{item.tratamiento_quirurgico} {item.ano}</li>
                                            ))}
                                        </ul>
                                    </div>
                                    : 
                                    <span><b>Antecedentes quirúrgicos: </b> Negados<br/></span>
                                }

                                <b>Antecedentes no patológicos: </b>
                                <ul>
                                    <li><b>Hábito tabáquico: </b>{this.props.antecedentespnp.habito_tabaquico}</li>
                                    <li><b>Hábito alcohólico: </b>{this.props.antecedentespnp.habito_alcoholico}</li>
                                    {this.props.transfusiones.length > 0 ? <li><b>Transfusiones: </b>{this.props.transfusiones.map((item) => (
                                        <span>{item.ano}. </span>
                                    ))}</li> : <li><b>Transfusiones: </b>Ninguna</li>}
                                    {this.props.alergias.length > 0 ? <li><b>Alergias: </b>{this.props.alergias.map((item) => (
                                        <span>{item.alergia}. </span>
                                    ))}</li> : <li><b>Alergias: </b>Ninguna</li>}
                                </ul>

                                {this.props.paciente.id_sexo == 'F' && 
                                <div>
                                <b>Antecedentes Gineco - Obstétricos: </b>
                                    <ul>
                                        <li>Gesta: {this.props.antecedentesgo.gesta}</li>
                                        <li>Menarca: {this.props.antecedentesgo.menarca}</li>
                                        <li>PARA: {this.props.antecedentesgo.para}</li>
                                        <li>FUM: {this.props.antecedentesgo.fum}</li>
                                        <li>Abortos: {this.props.antecedentesgo.abortos}</li>
                                        <li>Catamenio: {this.props.antecedentesgo.catamenio}</li>
                                        <li>IRS: {this.props.antecedentesgo.irs}</li>
                                    </ul>
                                </div>
                                }
                            </p>
                        </FormGroup>
                            
                        <div class="prueba-hr">
                            <h4 class="seccion">Padecimiento actual</h4>
                            <hr class="hr-dinamico"/>
                        </div>
                        <p>
                            {this.props.historialclinico.historia_enfermedad_actual}
                        </p>

                        <div class="prueba-hr">
                            <h4 class="seccion">Exploración física</h4>
                            <hr class="hr-dinamico"/>
                        </div>
                        <p>
                            <b>Signos vitales: </b>
                            <Table bordered responsive>
                                <center>
                                    <thead>
                                        <th>Talla</th>
                                        <th>Peso</th>
                                        <th>Temperatura</th>
                                        <th>Pulso</th>
                                        <th>Presion Arterial</th>
                                        
                                        {/* <th>Frecuencia Cardiaca</th> */}
                                        {/* <th>Frecuencia Respiratoria</th> */}
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{this.props.signosv.talla}</td>
                                            <td>{this.props.signosv.peso}</td>
                                            <td>{this.props.signosv.temperatura}</td>
                                            <td>{this.props.signosv.pulso}</td>
                                            <td>{this.props.signosv.pa}</td>
                                            {/* FALTA TEMPERATURA AXILAR */}
                                        </tr>
                                    </tbody>
                                </center>
                            </Table>
                        </p>
                        <b>Examen fisico segmentario: </b>
                        <Table responsive bordered striped>
                        <tbody>
                            {this.state.examenfisicodinamico.map((item, idx) => (
                                <tr key={'ef'+idx}>
                                    <th scope="row">{item.parte}</th>
                                    <td style={{textAlign: 'justify'}}>{item.descripcion}</td>
                                    <td><Button color="danger" onClick={this.eliminaExamenF(idx)}>Eliminar</Button></td>
                                </tr>
                            ))}
                        </tbody>
                        </Table>
                        <div class="boton-reiniciar">
                            <Button size="lg" onClick={this.reiniciaExamenF} color="warning">Reiniciar</Button>
                        </div>
                        

                        <div class="prueba-hr">
                            <h4 class="seccion">Diagnóstico</h4>
                            <hr class="hr-dinamico"/>
                        </div>
                        <p>
                            <span style={{backgroundColor: 'yellow'}}>{this.props.historialclinico.diagnostico_presuntivo}</span>
                        </p>

                        <hr class="diagnostico"/>
                        
                        <div class="prueba-hr">
                            <h4 class="seccion">Plan</h4>
                            <hr class="hr-dinamico"/>
                        </div>

                        <FormGroup row>
                            <Col md="12">
                                <Input 
                                    size="lg"
                                    type="textarea"
                                    rows="2"
                                    placeholder="Plan quirúrgico..."
                                    value={this.state.plan}
                                    onChange={e => this.setState({plan: e.target.value})}
                                />
                            </Col>
                        </FormGroup>
                        
                        <FormGroup row>
                            <Col md="2">
                                <h5>Programado para la fecha:</h5>
                            </Col>
                            <Col md="3">
                                <Input 
                                    type="date"
                                    size="lg"
                                    value={this.state.fechaprogramada}
                                    onChange={e => this.setState({fechaprogramada: e.target.value})}
                                />    
                            </Col>
                        </FormGroup>
                        
                        <div class="prueba-hr">
                            <h4 class="seccion">Indicaciones</h4>
                            <hr class="hr-dinamico"/>
                        </div>
                        {this.state.indicaciones.map((item, idx) => (
                            <FormGroup row key={'i'+idx}>
                                <Col md="12">
                                    <InputGroup size="lg">
                                        <InputGroupAddon addonType="prepend" style={{backgroundColor: '#005073', color: '#fff', padding: '0 15px', fontSize: '22px', borderRadius:'10px 0 0 10px'}}>
                                            {idx + 1}
                                        </InputGroupAddon>
                                        <Input 
                                            placeholder="Nueva indicación..."
                                            size="lg"
                                            type="text"
                                            value={item.indicacion}
                                            onChange={this.adicionaIndicacionSetState(idx)}
                                        />
                                        <InputGroupAddon addonType="append">
                                            <Button color="success" onClick={this.adicionaIndicacion}>+</Button>
                                        </InputGroupAddon>
                                        {idx > 0 &&
                                            <InputGroupAddon addonType="append">
                                                <Button color="danger" onClick={this.eliminaIndicacion(idx)}>-</Button>
                                            </InputGroupAddon>
                                        }
                                    </InputGroup>
                                </Col>
                            </FormGroup>
                        ))}
                    </CardBody>
                    <CardFooter>
                        <Button onClick={this.enviarDatos} size="lg" block color="info">Enviar</Button>
                    </CardFooter>
                </Card>
                </Col>
            </Row>
        </div>
        );
    }
}

NotaInternacionE.modules = {
    toolbar: [
        [{header: '1'}, {header: '2'}, {'font': []}],
        [{size: []}],
        ['bold', 'italic', 'underline', 'strike', 'blockquote'],
        [{'color': []}, {'background': []}, {'align': []}],
        [{'list': 'ordered'}, {'list': 'bullet'}],
        ['link', 'image'],
        ['clean'],
    ]
};

NotaInternacionE.formats = [
    'header', 'font', 'size',
    'bold', 'italic', 'underline', 'strike', 'blockquote',
    'color', 'background', 'align',
    'list', 'bullet',
    'link', 'image', 'video', 'code-block'
]

const mapStateToProps = state => ({
    paciente: state.paciente,
    historialclinico: state.historialclinico,
    anamnesis: state.anamnesis,
    examenf: state.examenf,
    laboratorios: state.laboratorios,
    antecedentespnp: state.antecedentespnp,
    antecedentespp: state.antecedentespp,
    antecedentesfam: state.antecedentesfam,
    planificacionf: state.planificacionf,
    antecedentesgo: state.antecedentesgo,
    transfusiones: state.transfusiones,
    alergias: state.alergias,
    antecedentesq: state.antecedentesq,
    antecedentesc: state.antecedentesc,
    signosv: state.signosv,
})

export default connect(mapStateToProps)(NotaInternacionE);