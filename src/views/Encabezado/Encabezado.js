import React, { Component } from 'react';

class Encabezado extends Component {
    constructor(props){
        super(props)
        this.state = {
        }
    }

    render() {
        return (
          <div className="animated fadeIn">
            <ul class="ul-flex">
              <li class="li-flexlogo">
                  <img src="assets/img/ssu.png" class="responsivo"/>
              </li>
              <li class="li-flextitulo">
                  <h1>SEGURO SOCIAL UNIVERSITARIO</h1>
                  <h2>La Paz - Bolivia</h2>
              </li>
              <li class="li-flexdescripcion">
                  <h3>PROMES</h3>
                  <h6>PROGRAMA MEDICO <br/> ESTUDIANTIL</h6>
              </li>
            </ul>
            <hr class="encabezado"/>
          </div>
        );
    }
}
export default Encabezado;