import React, { Component } from 'react';

import { connect } from 'react-redux';

class EncabezadoDatos extends Component {
    constructor(props){
        super(props)

        var hoy = new Date(),
            f = hoy.getFullYear() + '-' + (hoy.getMonth() + 1) + '-' + hoy.getDate();
        
        this.state = {
            fecha: f,
            hora: new Date(),
        }
    }

    render() {
        return (
          <div className="animated fadeIn">
              <p>
                <h5 class="nombre"><b>NOMBRE DEL ASEGURADO: </b>{this.props.paciente.primer_apellido + ' ' + this.props.paciente.segundo_apellido + ' ' + this.props.paciente.nombres}</h5>
                <ul class="ul-cmf">
                  <li class="li-cmf">
                    <h5><b>CI: </b> {this.props.paciente.dip}</h5>
                  </li>
                  <li class="li-cmf">
                    <h5><b>MATRICULA: </b> {this.props.paciente.matricula}</h5>
                  </li>
                  <li class="li-cmf">
                    <h5><b>SEXO: </b> {this.props.paciente.id_sexo}</h5>
                  </li>
                  <li class="li-cmf">
                    <h5><b>EDAD: </b> 1</h5>
                  </li>
                </ul>
                
                <ul class="ul-cmf">
                  <li class="li-cmf">
                    <h5><b>PISO: </b> [Sin asignar]</h5>
                  </li>
                  <li class="li-cmf">
                    <h5><b>SALA: </b> [Sin asignar]</h5>
                  </li>
                  <li class="li-cmf">
                    <h5><b>CAMA: </b> [Sin asignar]</h5>
                  </li>
                  <li class="li-cmf">
                    <h5><b>SERVICIO: </b> [Sin asignar]</h5>
                  </li>
                </ul>
              </p>
                <div class="thistoria"><h2><b>{this.props.titulo}</b></h2></div>
                <div class="fhistoria"><h5><span>Fecha de consulta: {this.state.fecha} </span></h5></div>
                <div class="fhistoria"><h5><span>Hora: {this.state.hora.toLocaleTimeString()} </span></h5></div>
          </div>
        );
    }
}

const mapStateToProps = state => ({
    paciente: state.paciente,
    historialclinico: state.historialclinico
})

export default connect(mapStateToProps)(EncabezadoDatos);