import React, { Component } from 'react';
import {
    Badge,
    Button,
    ButtonDropdown,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    Col,
    Collapse,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Fade,
    Form,
    FormGroup,
    FormText,
    FormFeedback,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Label,
    Row,
    Table,
    NavLink,
  } from 'reactstrap';
import Encabezado from '../Encabezado/Encabezado';

class HistoriaClinicaVer extends Component {
    constructor(props){
        super(props)

        this.state = {
        }
    }

   render() {
        console.log(this.props.antecedentesq);
        return (
            <div className="animated fadeIn">
                <Row>
                {/* <Col md="2"></Col> */}
                <Col xs="12" md="12">
                <Card>
                    <CardBody>
                        <Encabezado />
                        <h3><center>ANAMESIS</center></h3>
                      <h4><b>1. Filiación</b></h4>
                      <Table responsive borderless>
                          <tbody>
                            <tr>
                                <td><b>Nombre: </b>{this.props.nombre}</td>
                                <td><b>Matrícula: </b>{this.props.afiliadox.matricula}</td>
                            </tr>
                            <tr>
                                <td><b>Sexo: </b>{this.props.afiliadox.id_sexo}</td>
                                <td><b>Edad: </b>{this.props.edad}</td>
                            </tr>
                            <tr>
                                <td><b>Procedencia: </b>{this.props.afiliadox.sede}</td>
                                <td><b>Residencia: </b>{this.props.afiliadox.sede}</td>
                            </tr>
                            <tr>
                                <td><b>Pieza: </b>[Sin especificar]</td>
                                <td><b>Fecha de Internacion: </b>[Sin especificar]</td>
                            </tr>
                          </tbody>
                      </Table> 
                        <h4><b>2. Motivo de Internacion</b></h4>
                        <p>[Sin especificar]</p>

                        <h4><b>3. Historia de la Enfermedad actual</b></h4>
                        <p>{this.props.historiae}</p>

                        <h4><b>4. Antecedentes personales patológicos</b></h4>
                        
                        <FormGroup row>
                        <Col md="6">
                            <Table responsive>
                                <center>
                                    <h2>Clínicos</h2>
                                    <thead>
                                        <th>Clínicos</th>
                                    </thead>
                                    <tbody>
                                        {this.props.antecedentesc.map((item) => (
                                            <tr key={item.id}>
                                                <td>{item.antecedentec}</td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </center>
                            </Table>
                        </Col>
                        <Col md="6">
                            <Table responsive>
                                <center>
                                    <h2>Quirúrgicos</h2>
                                    <thead>
                                        <tr>
                                            <th>Tratamiento Quirúrgico</th>
                                            <th>Año</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.props.antecedentesq.map((item) => (
                                            <tr key={item.id}>
                                                <td>{item.tratamiento_quirurgico}</td>
                                                <td>{item.ano}</td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </center>
                            </Table>
                        </Col>
                        </FormGroup>
                        
                        <p>
                            <b>Alergias: </b>
                        </p>
                        {this.props.alergias.map((item) => (
                            <ul key={item.id}>
                                <li>{item.alergia}</li>
                            </ul>
                        ))}
                        <p>    
                            <b>Transfusiones: </b>  
                        </p>
                        {/* {this.props.transfusiones.map((item) => (
                            <ul key={item.id}>
                                <li>{item}</li>
                            </ul>
                        ))} */}

                        <h4><b>5. Antecedentes personales no Patológicos</b></h4><br/>

                        <p>
                            <b>Condiciones de vida: </b>
                        </p>
                        <ul>
                            <li><b>Vivienda: </b>{this.props.antecedentespnp.vivienda}</li>
                            <li><b>Servicio higienico intradomiciliario: </b>{this.props.antecedentespnp.servicio_higienico_intradomiciliario}</li>
                            <li><b>Agua intradomiciliara: </b>{this.props.antecedentespnp.agua_intradomiciliaria}</li>
                        </ul>
                        <p>
                            <b>Alimentación: </b> {this.props.antecedentespnp.alimentacion}<br/>
                            <b>Habito Tabáquico: </b> {this.props.antecedentespnp.habito_tabaquico}<br/>
                            <b>Habito Alcohólico: </b> {this.props.antecedentespnp.habito_alcoholico}<br/>
                            <b>Somnia: </b> {this.props.antecedentespnp.somnia}<br/>
                            <b>Diuresis: </b> {this.props.antecedentespnp.diuresis}<br/>
                            <b>Hábito Intestinal: </b> {this.props.antecedentespnp.habito_intestinal}<br/>
                        </p>

                        <p>
                            <h4><b>7. Antecedentes familiares</b></h4>
                            <Table responsive>
                                <center>
                                    <thead>
                                        <th>Antecedente</th>
                                        <th>Familiar</th>
                                    </thead>
                                    <tbody>
                                        {this.props.antecedentesf.map((item) => (
                                            <tr key={item.id}>
                                                <td>{item.antecedentef}</td>
                                                <td>{item.familiar}</td>
                                            </tr>
                                        ))}
                                    </tbody>
                                </center>
                            </Table>  
                            {this.props.sexo == 'F' &&
                                <div>
                                    <h4><b>9. Antecedentes gineco-obstetricos</b></h4>
                                        <Table row>
                                            <thead>
                                                <th>Gesta</th>
                                                <th>Menarca</th>
                                                <th>Para</th>
                                                <th>FUM</th>
                                                <th>Abortos</th>
                                                <th>Catamenio</th>
                                                <th>I.R.S.</th>
                                            </thead>
                                            <tbody>
                                                <td>{this.props.antecedentesgo.gesta}</td>
                                                <td>{this.props.antecedentesgo.menarca}</td>
                                                <td>{this.props.antecedentesgo.para}</td>
                                                <td>{this.props.antecedentesgo.fum}</td>
                                                <td>{this.props.antecedentesgo.abortos}</td>
                                                <td>{this.props.antecedentesgo.catamenio}</td>
                                                <td>{this.props.antecedentesgo.irs}</td>
                                            </tbody>
                                        </Table>
                                </div>
                            }
                        </p>

                        <center><h3><b>EXAMEN FÍSICO</b></h3></center>
                        <p>
                            <h4><b>1. Examen Físico General:</b></h4>
                            <center><h4>Signos vitales</h4></center>
                            <Table responsive>
                                <center>
                                    <thead>
                                        <th>Talla</th>
                                        <th>Peso</th>
                                        <th>Temperatura</th>
                                        <th>Pulso</th>
                                        <th>Presion Arterial</th>
                                        
                                        {/* <th>Frecuencia Cardiaca</th> */}
                                        {/* <th>Frecuencia Respiratoria</th> */}
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{this.props.signosv.talla}</td>
                                            <td>{this.props.signosv.peso}</td>
                                            <td>{this.props.signosv.temperatura}</td>
                                            <td>{this.props.signosv.pulso}</td>
                                            <td>{this.props.signosv.pa}</td>
                                            {/* FALTA TEMPERATURA AXILAR */}
                                        </tr>
                                    </tbody>
                                </center>
                            </Table>
                        </p>
                        <h4><b>2. Examen Físico Segmentario</b></h4>
                        <p>
                            <h5><b>Estado General</b></h5>
                            {this.props.examenf.estado_general}
                        </p>
                        <p>
                            <h5><b>Piel y faneras</b></h5>
                            {this.props.examenf.piel_faneras}
                        </p>
                        <p>
                            <h5><b>Cabeza y cuello</b></h5>
                            {this.props.examenf.cara_cuello}
                        </p>
                        <p>
                            <h5><b>Visual y Auditivo</b></h5>
                            {this.props.examenf.visual_auditivo}
                        </p>
                        <p>
                            <h5><b>Tiroides</b></h5>
                            {this.props.examenf.tiroides}
                        </p>
                        <p>
                            <h5><b>Cardiopulmonar</b></h5>
                            {this.props.examenf.cardiopulmonar}
                        </p>
                        <p>
                            <h5><b>Abdomen</b></h5>
                            {this.props.examenf.abdomen}
                        </p>
                        <p>
                            <h5><b>Genitales</b></h5>
                            {this.props.examenf.genitales}
                        </p>
                        <p>
                            <h5><b>Extremidades</b></h5>
                            {this.props.examenf.extremidades}
                        </p>
                        <p>
                            <h5><b>Neurologico</b></h5>
                            {this.props.examenf.neurologico}
                        </p>

                        <center><h3><b>IMPRESION DIAGNÓSTICA</b></h3></center>
                        <p>
                            {this.props.historiac.diagnostico_presuntivo}
                        </p>
                      
                    </CardBody>
                    <CardFooter>
                    {/* <Button onClick={this.handleOnSubmit} type="submit" size="sm" color="primary"><i className="fa fa-dot-circle-o"></i> Enviar</Button>
                    <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Limpiar</Button> */}
                    </CardFooter>
                </Card>
                
                </Col>
            </Row>

            {/* {this.state.show && <NotaInternacion afiliadox={this.state.afiliadox} id={this.state.id}/>} */}
        </div>
        );
    }
}
export default HistoriaClinicaVer;