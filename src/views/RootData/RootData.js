import React, { Component } from 'react';
import { Button, FormGroup, Row, Col} from 'reactstrap';

import NotaInternacionE from '../NotaInternacionE/NotaInternacionE';
import HistoriaClinicaVer from '../HistoriaClinicaVer/HistoriaClinicaVer';
import HojaDeReferencia from '../HojaDeReferencia/HojaDeReferencia';
import HojaEvolucion from '../HojaEvolucion/HojaEvolucion';
import NotasDeIndicaciones from '../NotasDeIndicaciones/NotasDeIndicaciones';
import HojaConsentimientoQ from '../HojaConsentimiento/HojaConsentimientoQ';
class RootData extends Component {
    constructor(props){
        super(props)
        this.state = {
            afiliadox: [],
            // -----------
            afiliadotel: [],
            // -----------
            historiac: [],
            // -----------
            anamnesis: [],
            examenf: [],
            laboratorios: [],
            // ----------
            antecedentespnp: [],
            antecedentespp: [],
            antecedentesfam: [],
            planificacionf: [],
            antecedentesgo: [],
            // -----------
            transfusiones: [],
            alergias: [],
            antecedentesq: [],
            antecedentesc: [],

            // -----------
            signosv: [],


            // =========================
            historiaclinicaver: false,
            notadeinternacionver: false,
            hojadereferenciaver: false,
            hojaevolucion: false,
            menuver: true,
        }
    }

    async componentDidMount() {
        try{
            const respuestaaf = await fetch(process.env.REACT_APP_API_URL+'api/v1/afiliado/'+this.props.match.params.dip+'/');
            const afiliadox = await respuestaaf.json();
            console.log(afiliadox);
            // ------------------------------------------------------------------------------
            const respuestataf = await fetch(process.env.REACT_APP_API_URL+'api/v1/afiliadotf/'+afiliadox.dip+'/');
            const afiliadotel = await respuestataf.json();
            console.log(afiliadotel);
            // ------------------------------------------------------------------------------
            const respuestahc = await fetch(process.env.REACT_APP_API_URL+'api/v1/historiaclinicaf/'+afiliadox.dip+'/');
            const historiac = await respuestahc.json();
            console.log(historiac);
            // ------------------------------------------------------------------------------
            const respuestaan = await fetch(process.env.REACT_APP_API_URL+'api/v1/anamnesisf/'+historiac.id_historiaclinica+'/');
            const anamnesis = await respuestaan.json();
            console.log(anamnesis);
            const respuestaef = await fetch(process.env.REACT_APP_API_URL+'api/v1/examenfisicof/'+historiac.id_historiaclinica+'/');
            const examenf = await respuestaef.json();
            console.log(examenf);
            const respuestalab = await fetch(process.env.REACT_APP_API_URL+'api/v1/laboratorios/');
            const laboratorios = await respuestalab.json();
            // PENDIENTE
            // ------------------------------------------------------------------------------
            const respuestaapnp = await fetch(process.env.REACT_APP_API_URL+'api/v1/antecedentespnpf/'+anamnesis.id_anamnesis+'/');
            const antecedentespnp = await respuestaapnp.json();
            console.log(antecedentespnp);
            const respuestaapp = await fetch(process.env.REACT_APP_API_URL+'api/v1/antecedentesppf/'+anamnesis.id_anamnesis+'/');
            const antecedentespp = await respuestaapp.json();
            console.log(antecedentespp);
            const respuestaafam = await fetch(process.env.REACT_APP_API_URL+'api/v1/antecedentesffk/'+anamnesis.id_anamnesis+'/');
            const antecedentesfam = await respuestaafam.json();
            console.log(antecedentesfam);
            const respuestapf = await fetch(process.env.REACT_APP_API_URL+'api/v1/planificacionf/');
            const planificacionf = await respuestapf.json();
            // Pendiente
            const respuestaago = await fetch(process.env.REACT_APP_API_URL+'api/v1/antecedentesgof/'+anamnesis.id_anamnesis+'/');
            const antecedentesgo = await respuestaago.json();
            console.log(antecedentesgo);
            // ------------------------------------------------------------------------------
            const respuestatr = await fetch(process.env.REACT_APP_API_URL+'api/v1/transfusionesf/'+antecedentespp.id_app+'/');
            const transfusiones = await respuestatr.json();
            console.log(transfusiones);
            const respuestaal = await fetch(process.env.REACT_APP_API_URL+'api/v1/alergiasf/'+antecedentespp.id_app+'/');
            const alergias = await respuestaal.json();
            console.log(alergias)
            const respuestaaq = await fetch(process.env.REACT_APP_API_URL+'api/v1/antecedentesqf/'+antecedentespp.id_app+'/');
            const antecedentesq = await respuestaaq.json();
            console.log(antecedentesq);
            const respuestaac = await fetch(process.env.REACT_APP_API_URL+'api/v1/antecedentescf/'+antecedentespp.id_app+'/');
            const antecedentesc = await respuestaac.json();
            console.log(antecedentesc);
            // ------------------------------------------------------------------------------
            const respuestasv = await fetch(process.env.REACT_APP_API_URL+'api/v1/signosvitalesef/'+examenf.id_examenfisico+'/');
            const signosv = await respuestasv.json();
            console.log(signosv);

            //Guardando los datos del afiliado
            this.setState({
                afiliadox,
                afiliadotel,
                historiac,
                anamnesis,
                examenf,
                laboratorios,
                antecedentespnp,
                antecedentespp,
                antecedentesfam,
                planificacionf,
                antecedentesgo,
                transfusiones,
                alergias,
                antecedentesq,
                antecedentesc,
                signosv
            });
              
        } catch(e){
            console.log(e);
        }
    }

    prueba1 = (e) =>{
        e.preventDefault();
        console.log('Afiliado');
        console.log(this.state.afiliadox);
        console.log('HistoriaC');
        console.log(this.state.historiac);
        console.log('Anamnesis');
        console.log(this.state.anamnesis);
        console.log('Examenf');
        console.log(this.state.examenf);
        console.log('laboratorios');
        console.log(this.state.laboratorios);
        console.log('antecedentespnp');
        console.log(this.state.antecedentespnp);
        console.log('antecedentespp');
        console.log(this.state.antecedentespp);
        console.log('antecedentesf');
        console.log(this.state.antecedentesfam);
        console.log('planificacionf');
        console.log(this.state.planificacionf);
        console.log('antecedentesgo');
        console.log(this.state.antecedentesgo);
        console.log('transfusiones');
        console.log(this.state.transfusiones);
        console.log('alergias');
        console.log(this.state.alergias);
        console.log('antecedentesq');
        console.log(this.state.antecedentesq);
        console.log('antecedentesc');
        console.log(this.state.antecedentesc);
        console.log('signosv');
        console.log(this.state.signosv);
    }

    render() {
        return (
            <div className="animated fadeIn">
                <Row>
                    <Col md="12">
                        <Button color="primary" onClick={e => this.setState({menuver: true, historiaclinicaver: false, notadeinternacionver: false, hojadereferenciaver: false, hojaevolucion: false})}>Volver</Button>
                        {this.state.menuver &&
                            <div>
                                <Button color="info" onClick={e => this.setState({historiaclinicaver : true, notadeinternacionver: false, hojadereferenciaver: false, menuver: false, hojaevolucion: false})}><img style={{ width: '300px'}} src="assets/img/examen.png"/></Button>
                                <Button color="danger" onClick={e => this.setState({notadeinternacionver : true, historiaclinicaver : false, hojadereferenciaver: false, menuver: false, hojaevolucion: false})}><img style={{ width: '300px'}} src="assets/img/examen.png"/></Button>
                                <Button color="warning" onClick={e => this.setState({hojadereferenciaver : true, historiaclinicaver: false, notadeinternacionver: false, menuver: false, hojaevolucion: false})}><img style={{ width: '300px'}} src="assets/img/examen.png"/></Button>
                                <Button color="primary" onClick={e => this.setState({hojadereferenciaver : false, historiaclinicaver: false, notadeinternacionver: false, menuver: false, hojaevolucion: true})}><img style={{ width: '300px'}} src="assets/img/examen.png"/></Button>
                            </div>
                        }
                            
                        {this.state.historiaclinicaver &&
                            <HistoriaClinicaVer 
                                afiliadox={this.state.afiliadox}
                                nombre={this.state.afiliadox.primer_apellido + ' ' + this.state.afiliadox.segundo_apellido + ' ' + this.state.afiliadox.nombres}
                                edad={this.state.afiliadox.fec_nacimiento}
                                historiae={this.state.historiac.historia_enfermedad_actual}
                                alergias={this.state.alergias}
                                transfusiones={this.state.transfusiones}
                                antecedentesf={this.state.antecedentesfam}
                                antecedentesc={this.state.antecedentesc}
                                antecedentesq={this.state.antecedentesq}
                                antecedentespnp={this.state.antecedentespnp}
                                antecedentesgo={this.state.antecedentesgo}
                                signosv={this.state.signosv}
                                examenf={this.state.examenf}
                                historiac={this.state.historiac}
                            />
                        }
                        
                        {this.state.notadeinternacionver &&
                            <NotaInternacionE 
                                afiliadox={this.state.afiliadox}
                                nombre={this.state.afiliadox.primer_apellido + ' ' + this.state.afiliadox.segundo_apellido + ' ' + this.state.afiliadox.nombres}
                                edad={this.state.afiliadox.fec_nacimiento}
                                antecedentesf={this.state.antecedentesfam}
                                antecedentesc={this.state.antecedentesc}
                                antecedentesq={this.state.antecedentesq}
                                antecedentespnp={this.state.antecedentespnp}
                                hintestinal={this.state.antecedentespnp.habito_intestinal}
                                historiac={this.state.historiac}
                                antecedentesc={this.state.antecedentesc}
                                antecedentesgo={this.state.antecedentesgo}
                                signosv={this.state.signosv}
                            />
                        }
                        
                        {this.state.hojadereferenciaver &&
                            <HojaDeReferencia 
                                afiliadox={this.state.afiliadox}
                                nombre={this.state.afiliadox.primer_apellido + ' ' + this.state.afiliadox.segundo_apellido + ' ' + this.state.afiliadox.nombres}
                                edad={this.state.afiliadox.fec_nacimiento}
                                historiac={this.state.historiac}
                                signosv={this.state.signosv}
                            />
                        }
                        
                        {this.state.hojaevolucion &&
                            <HojaEvolucion 
                                afiliadox={this.state.afiliadox}
                                nombre={this.state.afiliadox.primer_apellido + ' ' + this.state.afiliadox.segundo_apellido + ' ' + this.state.afiliadox.nombres}
                                edad={this.state.afiliadox.fec_nacimiento}
                            />
                        }
                        
                        <NotasDeIndicaciones 
                            afiliadox={this.state.afiliadox}
                            nombre={this.state.afiliadox.primer_apellido + ' ' + this.state.afiliadox.segundo_apellido + ' ' + this.state.afiliadox.nombres}
                            edad={this.state.afiliadox.fec_nacimiento}
                        />

                        <HojaConsentimientoQ />
                        
                        <Button onClick={this.prueba1} type="reset" size="lg" color="danger">
                            Limpiar
                        </Button>
                    </Col>
                </Row>
            </div>
            
        );
    }
}
export default RootData;