import React, { Component } from 'react';
import { Button, FormGroup, Row, Col} from 'reactstrap';

import NotaInternacionE from '../NotaInternacionE/NotaInternacionE';
import HistoriaClinicaVer from '../HistoriaClinicaVer/HistoriaClinicaVer';
import HojaDeReferencia from '../HojaDeReferencia/HojaDeReferencia';
class RootData extends Component {
    constructor(props){
        super(props)
        this.state = {
            sw: false,
            afiliadox: [],
            afiliadoxsw: true,
            // -----------
            // afiliadotel: [],
            // -----------
            historiac: [],
            historiacsw: false,
            // -----------
            anamnesis: [],
            anamnesissw: false,
            examenf: [],
            examenfsw: false,
            laboratorios: [],
            laboratoriossw: false,
            // ----------
            antecedentespnp: [],
            antecedentespnpsw: false,
            antecedentespp: [],
            antecedentesppsw: false,
            antecedentesfam: [],
            antecedentesfamsw: false,
            planificacionf: [],
            planificacionfsw: false,
            antecedentesgo: [],
            antecedentesgosw: false,
            // -----------
            transfusiones: [],
            transfusionessw: false,
            alergias: [],
            alergiassw: false,
            antecedentesq: [],
            antecedentesqsw: false,
            antecedentesc: [],
            antecedentescsw: false,

            // -----------
            signosv: [],
            signosvsw: false,


            // =========================
            historiaclinicaver: false,
            notadeinternacionver: false,
            hojadereferenciaver: false,
            menuver: true,
        }
    }

    async componentDidMount() {
        try{
            const respuestaaf = await fetch(process.env.REACT_APP_API_URL+'api/v1/afiliado/'+this.props.match.params.dip+'/');
            // ------------------------------------------------------------------------------
            // const respuestataf = await fetch(process.env.REACT_APP_API_URL+'api/v1/afiliadot/');
            // ------------------------------------------------------------------------------
            const respuestahc = await fetch(process.env.REACT_APP_API_URL+'api/v1/historiaclinica/');
            // ------------------------------------------------------------------------------
            const respuestaan = await fetch(process.env.REACT_APP_API_URL+'api/v1/anamnesis/');
            const respuestaef = await fetch(process.env.REACT_APP_API_URL+'api/v1/examenfisico/');
            const respuestalab = await fetch(process.env.REACT_APP_API_URL+'api/v1/laboratorios/');
            // ------------------------------------------------------------------------------
            const respuestaapnp = await fetch(process.env.REACT_APP_API_URL+'api/v1/antecedentespnp/');
            const respuestaapp = await fetch(process.env.REACT_APP_API_URL+'api/v1/antecedentespp/');
            const respuestaafam = await fetch(process.env.REACT_APP_API_URL+'api/v1/antecedentesf/');
            const respuestapf = await fetch(process.env.REACT_APP_API_URL+'api/v1/planificacionf/');
            const respuestaago = await fetch(process.env.REACT_APP_API_URL+'api/v1/antecedentesgo/');
            // ------------------------------------------------------------------------------
            const respuestatr = await fetch(process.env.REACT_APP_API_URL+'api/v1/transfusiones/');
            const respuestaal = await fetch(process.env.REACT_APP_API_URL+'api/v1/alergias/');
            const respuestaaq = await fetch(process.env.REACT_APP_API_URL+'api/v1/antecedentesq/');
            const respuestaac = await fetch(process.env.REACT_APP_API_URL+'api/v1/antecedentesc/');


            // ------------------------------------------------------------------------------
            const respuestasv = await fetch(process.env.REACT_APP_API_URL+'api/v1/signosvitales/');



            const afiliadox = await respuestaaf.json();
            // ------------------------------------------------------------------------------
            // const afiliadotel = await respuestataf.json();
            // ------------------------------------------------------------------------------
            const historiac = await respuestahc.json();
            // ------------------------------------------------------------------------------
            const anamnesis = await respuestaan.json();
            const examenf = await respuestaef.json();
            const laboratorios = await respuestalab.json();
            // ------------------------------------------------------------------------------
            const antecedentespnp = await respuestaapnp.json();
            const antecedentespp = await respuestaapp.json();
            const antecedentesf = await respuestaafam.json();
            const planificacionf = await respuestapf.json();
            const antecedentesgo = await respuestaago.json();
            // ------------------------------------------------------------------------------
            const transfusiones = await respuestatr.json();
            const alergias = await respuestaal.json();
            const antecedentesq = await respuestaaq.json();
            const antecedentesc = await respuestaac.json();


            // ------------------------------------------------------------------------------
            const signosv = await respuestasv.json();


            this.setState({
                afiliadox
            });

            // **********************************************************************************
            if(laboratorios.length == 0 || typeof laboratorios === 'undefined')
                this.setState({laboratoriossw: true});
            if(antecedentespnp.length == 0 || typeof antecedentespnp === 'undefined')
                this.setState({antecedentespnpsw: true});
            if(antecedentespp.length == 0 || typeof antecedentespp === 'undefined')
                this.setState({antecedentesppsw: true});
            if(antecedentesf.length == 0 || typeof antecedentesf === 'undefined')
                this.setState({antecedentesfsw: true});
            if(planificacionf.length == 0 || typeof planificacionf === 'undefined')
                this.setState({planificacionfsw: true});
            if(antecedentesgo.length == 0 || typeof antecedentesgo === 'undefined')
                this.setState({antecedentesgosw: true});
            if(transfusiones.length == 0 || typeof transfusiones === 'undefined')
                this.setState({transfusionessw: true});
            if(alergias.length == 0 || typeof alergias === 'undefined')
                this.setState({alergiassw: true});
            if(antecedentesq.length == 0 || typeof antecedentesq == 'undefined')
                this.setState({antecedentesqsw: true});
            if(antecedentesc.length == 0 || typeof antecedentesc == 'undefined')
                this.setState({antecedentescsw: true});
            if(signosv.length == 0 || typeof signosv == 'undefined')
                this.setState({signosvsw: true});
            // **********************************************************************************

            let dataaux=''
            historiac.map((item) => {
                if(this.props.match.params.dip == item.afiliado){
                    fetch(process.env.REACT_APP_API_URL+'api/v1/historiaclinica/'+item.id_historiaclinica+'/')
                    .then((Response)=>Response.json())
                    .then((findresponse) =>{
                        dataaux=findresponse.id_historiaclinica;
                        this.setState({
                            historiac: findresponse,
                            historiacsw: true
                        })
                    })
                    .then(() => {
                        let dataanamnesis=''
                        anamnesis.map((item) => {
                            if(dataaux == item.historia_clinica){
                                fetch(process.env.REACT_APP_API_URL+'api/v1/anamnesis/'+item.id_anamnesis+'/')
                                .then((Response) => Response.json())
                                .then((findresponse) => {
                                    dataanamnesis=findresponse.id_anamnesis;
                                    this.setState({
                                        anamnesis: findresponse,
                                        anamnesissw: true,
                                    })
                                })
                                .then(() => {
                                    antecedentespnp.map((item) => {
                                        if(dataanamnesis == item.anamnesis){
                                            fetch(process.env.REACT_APP_API_URL+'api/v1/antecedentespnp/'+item.id+'/')
                                            .then((Response) => Response.json())
                                            .then((findresponse) => {
                                                this.setState({
                                                    antecedentespnp: findresponse,
                                                    antecedentespnpsw: true,
                                                })
                                            });
                                        }
                                        return 0;
                                    })
                                })
                                .then(() => {
                                    let dataapp=''
                                    antecedentespp.map((item) => {
                                        if(dataanamnesis == item.anamnesis){
                                            fetch(process.env.REACT_APP_API_URL+'api/v1/antecedentespp/'+item.id_app+'/')
                                            .then((Response) => Response.json())
                                            .then((findresponse) => {
                                                dataapp = findresponse.id_app;
                                                this.setState({
                                                    antecedentespp: findresponse,
                                                    antecedentesppsw: true,
                                                })
                                            })
                                            .then(() => {
                                                let transfusionesv = [];
                                                transfusiones.map((item) => {
                                                    if(dataapp == item.app){
                                                        fetch(process.env.REACT_APP_API_URL+'api/v1/transfusiones/'+item.id+'/')
                                                        .then((Response) => Response.json())
                                                        .then((findresponse) => {
                                                            transfusionesv.push(findresponse)
                                                        })
                                                        .then(
                                                            this.setState({
                                                                transfusiones: transfusionesv,
                                                                transfusionessw: true
                                                            })
                                                        );
                                                    }
                                                    return 0;
                                                })
                                            })
                                            .then(() => {
                                                let alergiasv = [];
                                                alergias.map((item) => {
                                                    if(dataapp == item.app){
                                                        fetch(process.env.REACT_APP_API_URL+'api/v1/alergias/'+item.id+'/')
                                                        .then((Response) => Response.json())
                                                        .then((findresponse) => {
                                                            alergiasv.push(findresponse)
                                                        })
                                                        .then(
                                                            this.setState({
                                                                alergias: alergiasv,
                                                                alergiassw: true,
                                                            })
                                                        );
                                                    }
                                                    return 0;    
                                                })
                                            })
                                            .then(() => {
                                                let antecedentesqv = []
                                                antecedentesq.map((item) => {
                                                    if(dataapp == item.app){
                                                        fetch(process.env.REACT_APP_API_URL+'api/v1/antecedentesq/'+item.id+'/')
                                                        .then((Response) => Response.json())
                                                        .then((findresponse) => {
                                                            antecedentesqv.push(findresponse);
                                                        })
                                                        .then(
                                                            this.setState({
                                                                antecedentesq: antecedentesqv,
                                                                // antecedentesqsw: true,
                                                            })
                                                        );
                                                    }
                                                    return 0;
                                                });
                                                this.setState({
                                                    antecedentesqsw: true
                                                })
                                            })
                                            .then(() => {
                                                let antecedentescv = [];
                                                antecedentesc.map((item) => {
                                                    if(dataapp == item.app){
                                                        fetch(process.env.REACT_APP_API_URL+'api/v1/antecedentesc/'+item.id+'/')
                                                        .then((Response) => Response.json())
                                                        .then((findresponse) => {     
                                                            antecedentescv.push(findresponse);
                                                        })
                                                        .then(
                                                            this.setState({
                                                                antecedentesc: antecedentescv,
                                                                // antecedentescsw: true,
                                                            })
                                                        );
                                                    }
                                                    return 0;
                                                });
                                            });
                                        }
                                        return 0;
                                    })
                                })
                                .then(() => {
                                    let antecedentesfv = [];
                                    antecedentesf.map((item) => {
                                        console.log(dataanamnesis);
                                        console.log('?'+item.anamnesis);
                                        if(dataanamnesis == item.anamnesis){
                                            console.log(item.id);
                                            fetch(process.env.REACT_APP_API_URL+'api/v1/antecedentesf/'+item.id+'/')
                                            .then((Response) => Response.json())
                                            .then((findresponse) => {
                                                antecedentesfv.push(findresponse);
                                                
                                            })
                                            .then(
                                                this.setState({
                                                    antecedentesfam: antecedentesfv,
                                                    antecedentesfamsw: true,
                                                })
                                            );
                                        }
                                        return 0;
                                    })
                                })
                                .then(() => {
                                    planificacionf.map((item) => {
                                        if(dataanamnesis == item.anamnesis){
                                            fetch(process.env.REACT_APP_API_URL+'api/v1/planificacionf/'+item.id+'/')
                                            .then((Response) => Response.json())
                                            .then((findresponse) => {
                                                this.setState({
                                                    planificacionf: findresponse,
                                                    planificacionfsw: true,
                                                })
                                            });
                                        }
                                        return 0;
                                    })
                                })
                                .then(() => {
                                    antecedentesgo.map((item) => {
                                        if(dataanamnesis == item.anamnesis){
                                            fetch(process.env.REACT_APP_API_URL+'api/v1/antecedentesgo/'+item.id+'/')
                                            .then((Response) => Response.json())
                                            .then((findresponse) => {
                                                this.setState({
                                                    antecedentesgo: findresponse,
                                                    antecedentesgosw: true,
                                                })
                                            });
                                        }
                                        return 0;
                                    })
                                });
                            }
                            return 0;
                        });
                    })
                    .then(() => {
                        let dataexamenf=''
                        examenf.map((item) => {
                            if(dataaux == item.historia_clinica){
                                fetch(process.env.REACT_APP_API_URL+'api/v1/examenfisico/'+item.id_examenfisico+'/')
                                .then((Response) => Response.json())
                                .then((findresponse) => {
                                    dataexamenf=findresponse.id_examenfisico;
                                    this.setState({
                                        examenf: findresponse,
                                        examenfsw: true
                                    })
                                })
                                .then(() => {
                                    signosv.map((item) => {
                                        if(dataexamenf == item.examen_fisico){
                                            fetch(process.env.REACT_APP_API_URL+'api/v1/signosvitales/'+item.id_signosvitales+'/')
                                            .then((Response) => Response.json())
                                            .then((findresponse) => {
                                                this.setState({
                                                    signosv: findresponse,
                                                    signosvsw: true,
                                                })
                                            })
                                        }
                                        return 0;
                                    })
                                });
                            }
                            return 0;
                        })
                    })
                    .then(() => {
                        laboratorios.map((item) => {
                            if(dataaux == item.historia_clinica){
                                fetch(process.env.REACT_APP_API_URL+'api/v1/laboratorios/'+item.id+'/')
                                .then((Response) => Response.json())
                                .then((findresponse) => {
                                    this.setState({
                                        laboratorios: findresponse
                                    })
                                })
                            }
                        })
                    });        
                }
                return 0;
            });  
        } catch(e){
            console.log(e);
        }
    }

    prueba1 = (e) =>{
        e.preventDefault();
        console.log('Afiliado');
        console.log(this.state.afiliadox);
        console.log(this.state.afiliadoxsw);
        console.log('HistoriaC');
        console.log(this.state.historiac);
        console.log(this.state.historiacsw);
        console.log('Anamnesis');
        console.log(this.state.anamnesis);
        console.log(this.state.anamnesissw);
        console.log('Examenf');
        console.log(this.state.examenf);
        console.log(this.state.examenfsw);
        console.log('laboratorios');
        console.log(this.state.laboratorios);
        console.log(this.state.laboratoriossw);
        console.log('antecedentespnp');
        console.log(this.state.antecedentespnp);
        console.log(this.state.antecedentespnpsw);
        console.log('antecedentespp');
        console.log(this.state.antecedentespp);
        console.log(this.state.antecedentesppsw);
        console.log('antecedentesf');
        console.log(this.state.antecedentesfam);
        console.log(this.state.antecedentesfamsw);
        console.log('planificacionf');
        console.log(this.state.planificacionf);
        console.log(this.state.planificacionfsw);
        console.log('antecedentesgo');
        console.log(this.state.antecedentesgo);
        console.log(this.state.antecedentesgosw);
        console.log('transfusiones');
        console.log(this.state.transfusiones);
        console.log(this.state.transfusionessw);
        console.log('alergias');
        console.log(this.state.alergias);
        console.log(this.state.alergiassw);
        console.log('antecedentesq');
        console.log(this.state.antecedentesq);
        console.log(this.state.antecedentesqsw);
        console.log('antecedentesc');
        console.log(this.state.antecedentesc);
        console.log(this.state.antecedentescsw);
        console.log('signosv');
        console.log(this.state.signosv);
        console.log(this.state.signosvsw);
    }

    render() {
        return (
            <div className="animated fadeIn">
                <Row>
                    <Col md="12">
                        <Button color="primary" onClick={e => this.setState({menuver: true, historiaclinicaver: false, notadeinternacionver: false, hojadereferenciaver: false})}>Volver</Button>
                        {this.state.menuver &&
                            <div>
                                <Button color="info" onClick={e => this.setState({historiaclinicaver : true, notadeinternacionver: false, hojadereferenciaver: false, menuver: false})}><img style={{ width: '300px'}} src="assets/img/examen.png"/></Button>
                                <Button color="danger" onClick={e => this.setState({notadeinternacionver : true, historiaclinicaver : false, hojadereferenciaver: false, menuver: false})}><img style={{ width: '300px'}} src="assets/img/examen.png"/></Button>
                                <Button color="warning" onClick={e => this.setState({hojadereferenciaver : true, historiaclinicaver: false, notadeinternacionver: false, menuver: false})}><img style={{ width: '300px'}} src="assets/img/examen.png"/></Button>
                            </div>
                        }
                            
                        {this.state.historiaclinicaver &&
                            <HistoriaClinicaVer 
                                afiliadox={this.state.afiliadox}
                                nombre={this.state.afiliadox.primer_apellido + ' ' + this.state.afiliadox.segundo_apellido + ' ' + this.state.afiliadox.nombres}
                                edad={this.state.afiliadox.fec_nacimiento}
                                historiae={this.state.historiac.historia_enfermedad_actual}
                                alergias={this.state.alergias}
                                transfusiones={this.state.transfusiones}
                                antecedentesf={this.state.antecedentesfam}
                                antecedentesc={this.state.antecedentesc}
                                antecedentesq={this.state.antecedentesq}
                                antecedentespnp={this.state.antecedentespnp}
                                antecedentesgo={this.state.antecedentesgo}
                                signosv={this.state.signosv}
                                examenf={this.state.examenf}
                                historiac={this.state.historiac}
                            />
                        }
                        
                        {this.state.notadeinternacionver &&
                            <NotaInternacionE 
                                afiliadox={this.state.afiliadox}
                                nombre={this.state.afiliadox.primer_apellido + ' ' + this.state.afiliadox.segundo_apellido + ' ' + this.state.afiliadox.nombres}
                                edad={this.state.afiliadox.fec_nacimiento}
                                antecedentesf={this.state.antecedentesfam}
                                antecedentesc={this.state.antecedentesc}
                                antecedentesq={this.state.antecedentesq}
                                antecedentespnp={this.state.antecedentespnp}
                                hintestinal={this.state.antecedentespnp.habito_intestinal}
                                historiac={this.state.historiac}
                                antecedentesc={this.state.antecedentesc}
                                antecedentesgo={this.state.antecedentesgo}
                                signosv={this.state.signosv}
                            />
                        }
                        
                        {this.state.hojadereferenciaver &&
                            <HojaDeReferencia 
                                afiliadox={this.state.afiliadox}
                                nombre={this.state.afiliadox.primer_apellido + ' ' + this.state.afiliadox.segundo_apellido + ' ' + this.state.afiliadox.nombres}
                                edad={this.state.afiliadox.fec_nacimiento}
                                historiac={this.state.historiac}
                                signosv={this.state.signosv}
                            />
                        }
                        <Button onClick={this.prueba1} type="reset" size="lg" color="danger">
                            Limpiar
                        </Button>
                    </Col>
                </Row>
            </div>
            
        );
    }
}
export default RootData;