import React, { Component } from 'react';
import {
    Badge,
    Button,
    ButtonDropdown,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    Col,
    Collapse,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Fade,
    Form,
    FormGroup,
    FormText,
    FormFeedback,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Label,
    Row,
    Table,
  } from 'reactstrap';
import Encabezado from "../Encabezado/Encabezado";
import EncabezadoDatos from "../EncabezadoDatos/EncabezadoDatos";

import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';
import renderHTML from 'react-render-html';

import { connect } from 'react-redux';

class HojaDeReferencia extends Component {
    constructor(props){
        super(props)

        this.state = {
            sugerencias: '',
        }
        this.handleChangeSugerencias = this.handleChangeSugerencias.bind(this);
    }

    handleChangeSugerencias(value){
        this.setState({sugerencias: value})
    }

    render() {
        return (
            <div className="animated fadeIn" class="historiaclinica">
                <Row>
                <Col xs="12" md="12">
                <Card>
                    <CardBody>
                    <Encabezado />
                    <EncabezadoDatos titulo="HOJA DE REFERENCIA"/>
                        <p>
                            <h4><b>Motivo de Referencia</b></h4>
                            {this.props.historialclinico.diagnostico_presuntivo}
                        </p>

                        <p>
                            <h4><b>Sintomatología y evolución</b></h4>
                            {this.props.historialclinico.historia_enfermedad_actual}
                        </p>

                        <p>
                            <h4><b>Examen físico</b></h4>
                        </p>
                            <p>
                            <h5><b>Estado General</b></h5>
                            {this.props.examenf.estado_general}
                        </p>
                        <p>
                            <h5><b>Piel y faneras</b></h5>
                            {this.props.examenf.piel_faneras}
                        </p>
                        <p>
                            <h5><b>Cabeza y cuello</b></h5>
                            {this.props.examenf.cara_cuello}
                        </p>
                        <p>
                            <h5><b>Visual y Auditivo</b></h5>
                            {this.props.examenf.visual_auditivo}
                        </p>
                        <p>
                            <h5><b>Tiroides</b></h5>
                            {this.props.examenf.tiroides}
                        </p>
                        <p>
                            <h5><b>Cardiopulmonar</b></h5>
                            {this.props.examenf.cardiopulmonar}
                        </p>
                        <p>
                            <h5><b>Abdomen</b></h5>
                            {this.props.examenf.abdomen}
                        </p>
                        <p>
                            <h5><b>Genitales</b></h5>
                            {this.props.examenf.genitales}
                        </p>
                        <p>
                            <h5><b>Extremidades</b></h5>
                            {this.props.examenf.extremidades}
                        </p>
                        <p>
                            <h5><b>Neurologico</b></h5>
                            {this.props.examenf.neurologico}
                        </p>

                        <p>
                            <h4><b>Diagnóstico</b></h4>
                            <span style={{backgroundColor: 'yellow'}}>{this.props.historialclinico.diagnostico_presuntivo}</span>
                        </p>
                        

                        <p>
                            <h4><b>Tratamiento realizado</b></h4>
                            Ninguno
                        </p>

                        <p>
                            <h4><b>Laboratorio y otros examenes auxiliares de diagnóstico solicitados</b></h4>
                            [Sin asignar]
                        </p>

                        <p>
                            <h4><b>Sugerencias y comentarios</b></h4>
                            <Col sm={12}>
                                <ReactQuill 
                                    placeholder="Redactar..."
                                    modules={HojaDeReferencia.modules}
                                    formats={HojaDeReferencia.formats}
                                    value={this.state.sugerencias}
                                    onChange={this.handleChangeSugerencias}
                                />
                            </Col>
                        </p>
                    </CardBody>
                </Card>
                </Col>
            </Row>
        </div>
        );
    }
}

HojaDeReferencia.modules = {
    toolbar: [
        [{header: '1'}, {header: '2'}, {'font': []}],
        [{size: []}],
        ['bold', 'italic', 'underline', 'strike', 'blockquote'],
        [{'color': []}, {'background': []}, {'align': []}],
        [{'list': 'ordered'}, {'list': 'bullet'}],
        ['link', 'image'],
        ['clean'],
    ]
};

HojaDeReferencia.formats = [
    'header', 'font', 'size',
    'bold', 'italic', 'underline', 'strike', 'blockquote',
    'color', 'background', 'align',
    'list', 'bullet',
    'link', 'image', 'video', 'code-block'
]

const mapStateToProps = state => ({
    paciente: state.paciente,
    historialclinico: state.historialclinico,
    anamnesis: state.anamnesis,
    examenf: state.examenf,
    laboratorios: state.laboratorios,
    antecedentespnp: state.antecedentespnp,
    antecedentespp: state.antecedentespp,
    antecedentesfam: state.antecedentesfam,
    planificacionf: state.planificacionf,
    antecedentesgo: state.antecedentesgo,
    transfusiones: state.transfusiones,
    alergias: state.alergias,
    antecedentesq: state.antecedentesq,
    antecedentesc: state.antecedentesc,
    signosv: state.signosv,
})

export default connect(mapStateToProps)(HojaDeReferencia);