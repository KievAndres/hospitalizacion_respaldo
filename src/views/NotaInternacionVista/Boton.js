import React, { Component } from 'react';
import { Button } from "reactstrap";

export default class Boton extends Component {
  render() {
    return (
      <div>
        <Button type="submit" block color="primary">Ver Historial de Internación</Button>
      </div>
    )
  }
}
