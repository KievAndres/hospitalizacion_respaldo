import React, { Component } from 'react';
import {
    Badge,
    Button,
    ButtonDropdown,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    Col,
    Collapse,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Fade,
    Form,
    FormGroup,
    FormText,
    FormFeedback,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Label,
    Row,
    Table,
    NavLink,
  } from 'reactstrap';
  import Widget04 from './Widget04';
//   import NotaInternacion from '../NotaInternacion/NotaInternacion';


class Anamnesis extends Component {
    constructor(props){
        super(props)

        this.state = {
            anamnesis: [],
            cola_internacion: [],
            primary: false,
            show:false,
            data: [],
        }

        // this.handleOnSubmit = this.handleOnSubmit.bind(this)
        // this.handleChange = this.handleChange.bind(this)
        // this.togglePrimary = this.togglePrimary.bind(this)
    }

    async componentDidMount() {
        try{
            const respuesta = await fetch('http://127.0.0.1:8000/anamnesis/'+this.props.id+'/');
            const anamnesis = await respuesta.json();
            console.log(anamnesis)
            this.setState({
                anamnesis
            });
        } catch(e){
            console.log(e);
        }

        try{
            const respuesta = await fetch('http://127.0.0.1:8000/afiliados/'+this.props.id+'/');
            const cola_internacion = await respuesta.json();
            console.log(cola_internacion)
            this.setState({
                cola_internacion
            });
        } catch(e){
            console.log(e);
        }

        // try{
        //     const respuesta = await fetch('http://127.0.0.1:8000/anamnesis/');
        //     const anamnesis = await respuesta.json();
        //     console.log(anamnesis);
        //     console.log('Este es el match params: '+this.props.match.params.ci);
        //     anamnesis.map((item)=>{
        //         console.log(item);
        //         console.log(item.ci);
        //         if(this.props.match.params.ci == item.ci){
                    
        //             fetch('http://127.0.0.1:8000/anamnesis/'+item.id+'/')
        //             .then((Response) => Response.json())
        //             .then((findresponse) => {
        //                 this.setState({
        //                     anamnesis: findresponse
        //                 })
        //             });
        //         }
        //         return 0;
        //     });
        // } catch(e){
        //     console.log(e);
        // }

        // try{
        //     const respuesta = await fetch('http://127.0.0.1:8000/afiliados/');
        //     const cola_internacion = await respuesta.json();
        //     console.log(cola_internacion);
        //     console.log('Este es el match params: '+this.props.match.params.ci);
        //     cola_internacion.map((item)=>{
        //         console.log(item);
        //         console.log(item.ci);
        //         if(this.props.match.params.ci == item.ci){
                    
        //             fetch('http://127.0.0.1:8000/afiliados/'+item.id+'/')
        //             .then((Response) => Response.json())
        //             .then((findresponse) => {
        //                 this.setState({
        //                     cola_internacion: findresponse
        //                 })
        //             });
        //         }
        //         return 0;
        //     });
        // } catch(e){
        //     console.log(e);
        // }
    }


    updateSearch(event){
      this.setState({search: event.target.value.substr(0,20)});
  }

   render() {
        // console.log('Este es el match params: '+this.props.match.params.ci);

       console.log('-->'+this.state.anamnesis.id);
       console.log('-->'+this.state.cola_internacion.id);
        return (
            <div className="animated fadeIn">
                <Row>
                <Col xs="0" md="2">
                </Col>
                <Col xs="12" md="8">
                <Card>
                    <CardHeader>
                    
                        <center><h4><b>HOSPITAL DEL SEGURO SOCIAL UNIVERSITARIO LA PAZ</b></h4></center>
                        <center><h2><b>HISTORIA CLÍNICA</b></h2></center>
                        <center><h5><b>ANAMNESIS</b></h5></center>
                    
                    </CardHeader>
                    <CardBody>
                      
                      <h4><b>1. Filiación</b></h4>
                      <Table responsive borderless>
                          <tbody>
                            <tr>
                                <td><b>Nombre: </b>{this.state.cola_internacion.nombre1 +' '+this.state.cola_internacion.a_paterno+' '+this.state.cola_internacion.a_materno}</td>
                                <td><b>Matrícula: </b>678451</td>
                            </tr>
                            <tr>
                                <td><b>Sexo: </b>{this.state.cola_internacion.sexo}</td>
                                <td><b>Edad: </b>{this.state.cola_internacion.edad}</td>
                            </tr>
                            <tr>
                                <td><b>Procedencia: </b>La Paz</td>
                                <td><b>Residencia: </b>La Paz</td>
                            </tr>
                            <tr>
                                <td><b>Pieza: </b>{this.state.anamnesis.pieza}</td>
                                <td><b>Fecha de Internacion: </b>[Sin especificar]</td>
                            </tr>
                          </tbody>
                      </Table> 
                        <h4><b>2. Motivo de Internacion</b></h4>
                        <p>[Sin especificar]</p>

                        <h4><b>3. Historia de la Enfermedad actual</b></h4>
                        <p>[Sin especificar]</p>

                        <h4><b>4. Antecedentes personales patológicos</b></h4>
                        
                        <Table responsive>
                            <center>
                                <thead>
                                    <th>Clínicos</th>
                                    <th>Quirúrgicos</th>
                                </thead>
                                <tbody> 
                                    <td>{ this.state.anamnesis.antecedentes_clinicos }</td>
                                    <td>{ this.state.anamnesis.antecedentes_quirurgicos }</td>
                                </tbody>
                            </center>
                        </Table>
                        
                        <p>
                            <b>Alergias: </b> {this.state.anamnesis.alergias}<br/>
                            <b>Transfusiones: </b> {this.state.anamnesis.transfusiones}
                        </p>

                        <p>
                            <h4><b>5. Antecedentes personales no Patológicos</b></h4><br/>
                            <b>Condiciones de vida: </b> {this.state.anamnesis.condiciones_de_vida}<br/>
                            <b>Alimentación: </b> {this.state.anamnesis.alimentacion}<br/>
                            <b>Habito Tabáquico: </b> {this.state.anamnesis.habito_tabaquico}<br/>
                            <b>Habito Alcohólico: </b> {this.state.anamnesis.habito_alcoholico}<br/>
                            <b>Somnia: </b> {this.state.anamnesis.somnia}<br/>
                            <b>Diuresis: </b> {this.state.anamnesis.diuresis}<br/>
                            <b>Hábito Intestinal: </b> {this.state.anamnesis.habito_intestinal}<br/>
                        </p>

                        <p>
                            <h4><b>7. Antecedentes familiares</b></h4>
                            { this.state.anamnesis.antecedentes_familiares }  
                            <h4><b>8. Anamnesis por sistemas</b></h4>
                            { this.state.anamnesis.anamnesis_por_sistemas }  
                            <h4><b>9. Antecedentes gineco-obstetricos</b></h4>
                            { this.state.anamnesis.antecedentes_g }  
                        </p>

                        <center><h3><b>EXAMEN FÍSICO</b></h3></center>
                        <p>
                            <h4><b>1. Examen Físico General:</b></h4>
                            { this.state.anamnesis.examen_fisico }
                            <center><h4>Signos vitales</h4></center>
                            <Table responsive>
                                <center>
                                    <thead>
                                        <th>Presion Arterial</th>
                                        <th>Frecuencia Cardiaca</th>
                                        <th>Frecuencia Respiratoria</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>{this.state.anamnesis.presion_arterial}</td>
                                            <td>{this.state.anamnesis.frecuencia_cardiaca}</td>
                                            <td>{this.state.anamnesis.frecuencia_respiratoria}</td>
                                            {/* FALTA TEMPERATURA AXILAR */}
                                        </tr>
                                    </tbody>
                                </center>
                            </Table>
                        </p>
                        <h4><b>2. Examen Físico Segmentario</b></h4>
                        <p>
                            <h5><b>Cabeza y cuello</b></h5>
                            {this.state.anamnesis.cabeza_cuello}
                        </p>
                        <p>
                            <h5><b>Tórax</b></h5>
                            {this.state.anamnesis.torax}
                        </p>
                        <p>
                            <h5><b>Abdomen</b></h5>
                            {this.state.anamnesis.abdomen}
                        </p>
                        <p>
                            <h5><b>Genitourinario</b></h5>
                            {this.state.anamnesis.genitourinario}
                        </p>
                        <p>
                            <h5><b>Extremidades</b></h5>
                            {this.state.anamnesis.extremidades}
                        </p>
                        <p>
                            <h5><b>Neurológico</b></h5>
                            {this.state.anamnesis.neurologico}
                        </p>

                        <center><h3><b>IMPRESION DIAGNÓSTICA</b></h3></center>
                        {this.state.anamnesis.impresion_diagnostica}
                      
                    </CardBody>
                    <CardFooter>
                    {/* <Button onClick={this.handleOnSubmit} type="submit" size="sm" color="primary"><i className="fa fa-dot-circle-o"></i> Enviar</Button>
                    <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Limpiar</Button> */}
                    </CardFooter>
                </Card>
                
                </Col>
            </Row>

            {/* {this.state.show && <NotaInternacion afiliadox={this.state.afiliadox} id={this.state.id}/>} */}
        </div>
        );
    }
}
export default Anamnesis;