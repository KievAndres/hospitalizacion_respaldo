import React, { Component } from 'react';
import {
    Badge,
    Button,
    ButtonDropdown,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    Col,
    Collapse,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Fade,
    Form,
    FormGroup,
    FormText,
    FormFeedback,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Label,
    Pagination, 
    PaginationItem, 
    PaginationLink,
    Row,
    Table,
    NavLink,
  } from 'reactstrap';
  import Widget04 from '../NotaInternacionVista/Widget04';
  import NotaInternacion from '../NotaInternacion/NotaInternacion';


class Anamnesis extends Component {
    constructor(props){
        super(props)

        this.state = {
            internados: [],
            primary: false,
            Nombre:'',
            Direccion:'',
            Telefono: '',
            Fecha:'',
            Hora:'',
            Descripcion:'',
            Diagnostico:'',
            Indicaciones:'',

            id:'',
            show:false,
            afiliadox: [],

            search: '',
            data: [],
        }

        // this.handleOnSubmit = this.handleOnSubmit.bind(this)
        // this.handleChange = this.handleChange.bind(this)
        // this.togglePrimary = this.togglePrimary.bind(this)
    }

    async componentDidMount() {
        try{
            const respuesta = await fetch('http://127.0.0.1:8000/afiliados/');
            const internados = await respuesta.json();
            console.log(internados)
            this.setState({
                internados
            });
        } catch(e){
            console.log(e);
        }
    }

    tabla_boton = (e, id) => {
      e.preventDefault();
      this.state.id = id;
      console.log(this.state.id);
      this.state.internados.map((item, indice)=>{
             
        if(id===item.id){
            console.log(item); 
            fetch('http://127.0.0.1:8000/afiliados/'+item.id+'/')
            .then((Response)=>Response.json())
            .then((findresponse) => {
            this.setState({
              afiliadox: findresponse,
              show: true
            })
        });
        }
        return 0;
      })
    }

    updateSearch(event){
      this.setState({search: event.target.value.substr(0,20)});
  }

  

   render() {
        let afiliados_filtrados = this.state.internados.filter(
          (item) => {
              return item.nombre1.toUpperCase().indexOf(
                this.state.search.toUpperCase()) !== -1; 
          }
        );     
        return (
            <div className="animated fadeIn">
                <Row>     
                    <h2>Selecciona los estudiantes a hospitalizar</h2>

                <h1>
                    <Col md="5">
                        <ul>
                            <input type="text"
                            value={ this.state.search }
                            onChange={ this.updateSearch.bind(this) }
                            />
                        </ul>
                    </Col>
                </h1>

                <Col xl="10">
                    <Card>
                    <CardHeader>
                        <i className="fa fa-align-justify"></i> Simple Table
                    </CardHeader>
                    <CardBody>
                        <Table responsive>
                        <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Carnet de Identidad</th>
                            <th>Fecha de Consulta</th>
                            <th>Estado</th>
                        </tr>
                        </thead>
                        <tbody>
                        {afiliados_filtrados.map((item)=>(
                            <tr key={item.id}>
                                <td>{item.a_paterno} {item.a_materno} {item.nombre1}</td>
                                <td>{item.ci}</td>
                                <td>{item.fecha_nacimiento}</td>
                                <td><Badge color="success">Estable</Badge></td>
                                <NavLink href={'#/colainternacion/menuinternacion/' + item.ci} >
                                    <Button size="lg" outline color="success">Ver</Button>
                                </NavLink>
                            </tr>
                        ))}              
                        </tbody>
                        </Table>
                        <Pagination>
                        <PaginationItem>
                            <PaginationLink previous tag="button"></PaginationLink>
                        </PaginationItem>
                        <PaginationItem active>
                            <PaginationLink tag="button">1</PaginationLink>
                        </PaginationItem>
                        <PaginationItem>
                            <PaginationLink tag="button">2</PaginationLink>
                        </PaginationItem>
                        <PaginationItem>
                            <PaginationLink tag="button">3</PaginationLink>
                        </PaginationItem>
                        <PaginationItem>
                            <PaginationLink tag="button">4</PaginationLink>
                        </PaginationItem>
                        <PaginationItem>
                            <PaginationLink next tag="button"></PaginationLink>
                        </PaginationItem>
                        </Pagination>
                    </CardBody>
                    </Card>
                </Col>
                

                <Col xl="2">
                    <Widget04 icon="icon-people" color="info" header={ this.state.internados.length } value="75">Internados</Widget04>
                    <Widget04 icon="icon-people" color="success" header={ afiliados_filtrados.length } value="75" invert>Filtrados</Widget04>
                </Col>  

                
            </Row>

            {/* {this.state.show && <NotaInternacion afiliadox={this.state.afiliadox} id={this.state.id}/>} */}
        </div>
        );
    }
}
export default Anamnesis;