import React, { Component } from 'react';
import {
    Badge,
    Button,
    ButtonDropdown,
    Card,
    CardBody,
    CardFooter,
    CardHeader,
    Col,
    Collapse,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Fade,
    Form,
    FormGroup,
    FormText,
    FormFeedback,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Label,
    Row,
    Table,
  } from 'reactstrap';
  import Widget04 from '../../NotaInternacionVista/Widget04';


class HojaDeReferencia extends Component {
    constructor(props){
        super(props)

        this.state = {
            internados: [],
            anamnesis: [],
            primary: false,
            Nombre:'',
            Direccion:'',
            Telefono: '',
            Fecha:'',
            Hora:'',
            Descripcion:'',
            Diagnostico:'',
            Indicaciones:'',

            search: '',
            data: [],
        }

        this.handleOnSubmit = this.handleOnSubmit.bind(this)
        this.handleChange = this.handleChange.bind(this)
        this.togglePrimary = this.togglePrimary.bind(this)
    }

    async componentDidMount() {
        // try{
        //     const respuesta = await fetch('http://127.0.0.1:8000/afiliados/'+this.props.match.params.id+'/');
        //     const internados = await respuesta.json();
        //     console.log(internados)
        //     this.setState({
        //         internados
        //     });
        // } catch(e){
        //     console.log(e);
        // }

        // try{
        //     const respuesta = await fetch('http://127.0.0.1:8000/anamnesis/'+this.props.match.params.id+'/');
        //     const anamnesis = await respuesta.json();
        //     console.log(anamnesis)
        //     this.setState({
        //         anamnesis
        //     });
        // } catch(e){
        //     console.log(e);
        // }
        try{
            const respuesta = await fetch('http://127.0.0.1:8000/anamnesis/');
            const anamnesis = await respuesta.json();
            console.log(anamnesis);
            console.log('Este es el match params: '+this.props.match.params.ci);
            anamnesis.map((item)=>{
                console.log(item);
                console.log(item.ci);
                if(this.props.match.params.ci == item.ci){
                    
                    fetch('http://127.0.0.1:8000/anamnesis/'+item.id+'/')
                    .then((Response) => Response.json())
                    .then((findresponse) => {
                        this.setState({
                            anamnesis: findresponse
                        })
                    });
                }
                return 0;
            });
        } catch(e){
            console.log(e);
        }

        try{
            const respuesta = await fetch('http://127.0.0.1:8000/afiliados/');
            const internados = await respuesta.json();
            console.log(internados);
            console.log('Este es el match params: '+this.props.match.params.ci);
            internados.map((item)=>{
                console.log(item);
                console.log(item.ci);
                if(this.props.match.params.ci == item.ci){
                    
                    fetch('http://127.0.0.1:8000/afiliados/'+item.id+'/')
                    .then((Response) => Response.json())
                    .then((findresponse) => {
                        this.setState({
                            internados: findresponse
                        })
                    });
                }
                return 0;
            });
        } catch(e){
            console.log(e);
        }
    }

    handleChange(value){    
      console.log(value);
      this.setState({ muestraBoton: true });
    }

    handleOnSubmit(e){
      console.log(this.state.Telefono)
      alert(
        'Hola: ' + this.state.Nombre + 
        '\nDireccion: ' + this.state.Direccion + 
        '\nTelefono: ' + this.state.Telefono +
        '\nFecha: ' + this.state.Fecha +
        '\nHora: ' + this.state.Hora +
        '\nDescripcion ' + this.state.Descripcion +
        '\nDiagnostico ' + this.state.Diagnostico +
        '\nIndicaciones ' + this.state.Indicaciones
      );
      e.preventDefault();

      let url =''
      let data = {}

      url = 'http://127.0.0.1:8000/resumens/'
      data = {
          nombre: this.state.Nombre,
          direccion: this.state.Direccion,
          telefono: this.state.Telefono,
          fecha: this.state.Fecha,
          hora: this.state.Hora,
          descripcion: this.state.Descripcion,
          diagnostico: this.state.Diagnostico,
          indicaciones: this.state.Indicaciones
      }

      try{
        fetch(url, {
          method: 'POST',
          body: JSON.stringify(data),
          headers:{
            'Content-Type': 'application/json'
          }
        }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => console.log('Succes:', response));
      } catch(e){
        console.log(e)
      }

      this.setState({
        primary: !this.state.primary,
      });

      // e.target.reset();
    }

    togglePrimary() {
        this.setState({
            primary: !this.state.primary,
        });
    }

    updateSearch(event){
      this.setState({search: event.target.value.substr(0,20)});
  }

   render() {
        return (
            <div className="animated fadeIn">
                <Row>
                <Col xs="0" md="2">
                </Col>
                <Col xs="12" md="8">
                <Card>
                    <CardHeader>
                    <center>
                        <h1><b>Hoja de Referencia</b></h1>
                        <h4><b>PROMES - SSU</b></h4>
                    </center>
                    </CardHeader>
                    <CardBody>
                    <Form action="" method="post" encType="multipart/form-data" className="form-horizontal">
                        <font size="3">
                            <p>
                               <b>Nombre del asegurado o beneficiario.- </b>{this.state.internados.a_paterno} {this.state.internados.a_materno} {this.state.internados.nombre1}<br/>
                               <b>Matrícula - Asegurado. </b>678451 <br/>
                               <b>Nombre y sello del médico solicitante</b> Dr. Roger Pozzo <br/>
                               <b>Expecialidd a la que es referido el paciente</b> Cirugía <br/>
                            </p>

                            <p>
                                <h4><b>Antecedentes de importancia</b></h4>
                                {/* DIAGNOSTICO */}
                            </p>

                            <p>
                                <h4><b>Sintomatología y evolución</b></h4>
                                <Col sm={12}>
                                    <Input type="textarea" placeholder="Diagnóstico del médico..." bgsize="lg" onChange={e => this.setState({ Hora: e.target.value })}/>
                                </Col>
                            </p>

                            <p>
                                <h4><b>Examen físico</b></h4>
                                <Table responsive>
                                    <center>
                                        <thead>
                                            <th>Presion Arterial</th>
                                            <th>Frecuencia Cardiaca</th>
                                            <th>Frecuencia Respiratoria</th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{this.state.anamnesis.presion_arterial}</td>
                                                <td>{this.state.anamnesis.frecuencia_cardiaca}</td>
                                                <td>{this.state.anamnesis.frecuencia_respiratoria}</td>
                                                {/* FALTA TEMPERATURA AXILAR */}
                                            </tr>
                                        </tbody>
                                    </center>
                                </Table>
                            </p>

                            <p>
                                <h4><b>Tratamiento realizado</b></h4>
                                {/* Tratamiento */}
                            </p>

                            <p>
                                <h4><b>Laboratorio y otros examenes auxiliares de diagnóstico solicitados</b></h4>
                                {/* Laboratorio */}
                            </p>

                            <p>
                                <h4><b>Sugerencias y comentarios</b></h4>
                                <Col sm={12}>
                                    <Input type="textarea" placeholder="Diagnóstico del médico..." bgsize="lg" onChange={e => this.setState({ Hora: e.target.value })}/>
                                </Col>
                            </p>
                        </font>
                        
                    </Form>
                    </CardBody>
                    <CardFooter>
                    <Button onClick={this.handleOnSubmit} type="submit" size="sm" color="primary"><i className="fa fa-dot-circle-o"></i> Enviar</Button>
                    <Button type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Limpiar</Button>
                    </CardFooter>
                </Card>
                </Col>
            </Row>
        </div>
        );
    }
}
export default HojaDeReferencia;