import React, { Component } from 'react';
import {
    Badge,
    Button,
    ButtonDropdown,
    Card,
    CardTitle,
    CardText,
    CardBody,
    CardFooter,
    CardHeader,
    Col,
    Collapse,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Fade,
    Form,
    FormGroup,
    FormText,
    FormFeedback,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Label,
    Row,
    Table,
    NavLink,
  } from 'reactstrap';

class PacienteNoAsignado extends Component {
    constructor(props){
        super(props)
        this.state = {

        }
    }

    render() {        
        return (
            <div className="animated fadeIn">
                <Card>
                    <div class="user-notfound">
                        <div class="encabezado">
                            <h1>Aún no has escogido un paciente</h1>
                        </div>
                        <div class="boton">
                            <NavLink href="#/listapacientes"><span>Comenzar ahora</span></NavLink>
                        </div>
                    </div>
                </Card>
            </div>
        );
    }
}
export default PacienteNoAsignado;