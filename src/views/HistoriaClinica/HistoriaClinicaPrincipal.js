import React, { Component } from 'react';
import {
    Badge,
    Button,
    ButtonDropdown,
    Card,
    CardTitle,
    CardText,
    CardBody,
    CardFooter,
    CardHeader,
    Col,
    Collapse,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Fade,
    Form,
    FormGroup,
    FormText,
    FormFeedback,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Label,
    Row,
    Table,
    NavLink,
  } from 'reactstrap';
import HistoriaClinica from './HistoriaClinica';
import HistoriaClinicaVer from './HistoriaClinicaVer';
import PacienteNoAsignado from '../NoCreado/PacienteNoAsignado';
import HCNoCreada from '../NoCreado/HCNoCreada';

import { connect } from 'react-redux';

class HistoriaClinicaPrincipal extends Component {
    constructor(props){
        super(props)
        this.state = {
            historiasw: false,
        }
        this.handleHistoriaSw = this.handleHistoriaSw.bind(this);
    }

    handleHistoriaSw(sw){
        this.setState({
            historiasw: sw
        })
    }

    render() {
        console.log(this.state.historiasw);
        return (
            <div className="animated fadeIn">
                {this.props.user == 'Indefinido' ?
                    <PacienteNoAsignado />
                    :
                    <div>
                        {this.props.hc ?
                            <HistoriaClinicaVer paciente={this.props.paciente}/>
                            :
                            <div>
                                {this.state.historiasw == false ?
                                    <HCNoCreada
                                        historiasw={this.state.historiasw}
                                        onHistoriaSw={this.handleHistoriaSw}
                                    />
                                    :
                                    <HistoriaClinica/>
                                }
                            </div>
                        }
                    </div>
                }
                {/* <HistoriaClinica user={this.props.user} /> */}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    user: state.user,
    paciente: state.paciente,
    hc: state.hc
})

export default connect(mapStateToProps)(HistoriaClinicaPrincipal);