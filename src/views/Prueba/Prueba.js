import React, { Component } from 'react';
import {
    Badge,
    Button,
    ButtonDropdown,
    Card,
    CardTitle,
    CardText,
    CardBody,
    CardFooter,
    CardHeader,
    Col,
    Collapse,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Fade,
    Form,
    FormGroup,
    FormText,
    FormFeedback,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Label,
    Row,
    Table,
    NavLink,
  } from 'reactstrap';

import HistoriaClinica from '../HistoriaClinica/HistoriaClinica';
import HojaEvolucion from '../HojaEvolucion/HojaEvolucion';
import ResumenDeAlta from '../ResumenDeAlta/ResumenDeAlta';
import NotaInternacionE from '../NotaInternacionE/NotaInternacionE';
import HojaDeReferencia from '../HojaDeReferencia/HojaDeReferencia';
import NotasDeIndicaciones from '../NotasDeIndicaciones/NotasDeIndicaciones';
import HojaConsentimientoQ from '../HojaConsentimiento/HojaConsentimientoQ';

import { connect } from 'react-redux';
import { updateUser} from '../Redux/Actions/UserActions';
import jsPDF from 'jspdf';

class Prueba extends Component {
    constructor(props){
        super(props);
        this.state = {
            nombre:'',
            pesan:'',
            tinggi:11.69,
            lebar:'08.27',
            judul:'Lintang.pdf',
        }
    }

    crearPdf(e){
        e.preventDefault();

        var doc = new jsPDF({
            unit: 'in',
            format: [this.state.tinggi, this.state.lebar]
        })

        doc.text(`hola mundo`, 0.5, 0.5)

        doc.save(`${this.state.judul}`)
    }

    render() {
        var doc = new jsPDF({

        })       
        return (
            <div className="animated fadeIn">
                <Card>
                    <HistoriaClinica />
                </Card>
            </div>
        );
    }
}
const mapStateToProps = state => ({
    user: state.user
});
    
const mapActionsToProps = {
    onUpdateUser: updateUser
};
export default connect(mapStateToProps, mapActionsToProps)(Prueba);