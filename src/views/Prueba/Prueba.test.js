import React, { Component } from 'react';
import {
    Badge,
    Button,
    ButtonDropdown,
    Card,
    CardTitle,
    CardText,
    CardBody,
    CardFooter,
    CardHeader,
    Col,
    Collapse,
    DropdownItem,
    DropdownMenu,
    DropdownToggle,
    Fade,
    Form,
    FormGroup,
    FormText,
    FormFeedback,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText,
    Label,
    Row,
    Table,
    NavLink,
  } from 'reactstrap';
import { Provider } from "react-redux";
import store from "../Redux/store/index";
import Prueba from "./Prueba";
class PruebaTest extends Component {
    constructor(props){
        super(props)
        this.state = {

        }
    }

    render() {        
        return (
            <div className="animated fadeIn">
                <Provider store={store}>
                    <Prueba />
                </Provider>
            </div>
        );
    }
}
export default PruebaTest;