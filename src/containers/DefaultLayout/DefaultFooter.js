import React, { Component } from 'react';
import PropTypes from 'prop-types';

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

class DefaultFooter extends Component {
  render() {

    // eslint-disable-next-line
    const { children, ...attributes } = this.props;

    return (
      <React.Fragment>
        <span style={{color: '#005073'}}><a href="/historiaclinica"><img src="./assets/img/logo.svg" class="responsivog" style={{maxWidth: '90px'}}></img></a> &copy; 2018 SI - GESHO - PROMES.</span>
        <span style={{color: '#005073'}} className="ml-auto">Desarrollado por: <a href="#">Kiev Andres Mendoza Rojas</a></span>
      </React.Fragment>
    );
  }
}

DefaultFooter.propTypes = propTypes;
DefaultFooter.defaultProps = defaultProps;

export default DefaultFooter;
