import React, { Component } from 'react';
import { Badge, DropdownItem, DropdownMenu, DropdownToggle, Modal, ModalBody, ModalHeader, ModalFooter, Nav, NavItem, NavLink, Popover, PopoverHeader, PopoverBody } from 'reactstrap';
import PropTypes from 'prop-types';

import { AppAsideToggler, AppHeaderDropdown, AppNavbarBrand, AppSidebarToggler } from '@coreui/react';
import logo from '../../assets/img/brand/logo.svg'
import sygnet from '../../assets/img/brand/sygnet.svg'

const propTypes = {
  children: PropTypes.node,
};

const defaultProps = {};

class DefaultHeader extends Component {
    constructor(props){
        super(props);
        this.state = {
            popoverOpen: false,
            historiac: [],
            modal: false
        }
        this.toggle = this.toggle.bind(this);
        this.toggleModal = this.toggleModal.bind(this);
    }
    toggle(){
        this.setState({
            popoverOpen: !this.state.popoverOpen,
        });
    }
    toggleModal(){
        this.setState({
            modal: !this.state.modal,
            popoverOpen: false
        })
    }
  render() {
    let lowernombre = this.props.nombre;
    lowernombre = lowernombre.toLowerCase();
    console.log(lowernombre);

    // eslint-disable-next-line
    const { children, ...attributes } = this.props;

    return (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        <AppNavbarBrand
          full={{ src: logo, alt: 'Hospitalizacion' }}
          minimized={{ src: sygnet, width: 30, height: 30, alt: 'CoreUI Logo' }}
        />
        <AppSidebarToggler className="d-md-down-none" display="lg" />

        <Nav className="d-md-down-none" navbar>
          {this.props.dip != 'Indefinido' &&
            <NavItem className="px-3">
                <NavLink href="#/listapacientes" class="escoje-boton"><span class="escoje-boton">Cambiar</span></NavLink>
            </NavItem>
          }
          <NavItem className="px-3">
            {this.props.dip != 'Indefinido' ? 
                <div className="animated fadeIn" class="escoje-usuario">
                    <NavLink id="Popover1" class="navlink"><div><span onClick={this.toggle}>{lowernombre}</span><span onClick={this.toggleModal} class="ver-mas">Ver Datos Personales</span></div></NavLink>
                    <Modal isOpen={this.state.modal} toggle={this.toggleModal} className={'modal-lg ' + this.props.className}>
                      <ModalHeader toggle={this.toggleModal} class="modal-header">
                        <h4 class="seccion">{this.props.nombre}</h4>
                      </ModalHeader>
                      <ModalBody>
                        <div class="modal-body">
                            <p style={{textAlign: "center"}}>
                                <b>Documento de Identificación Personal: </b>{this.props.paciente.dip}<br/>
                                <b>Matrícula: </b>{this.props.paciente.matricula}<br/>
                            </p>
                            <hr/>
                            <p>
                                {this.props.paciente.id_sexo == 'F' ? 
                                    <span><b>Sexo: </b>Femenino<br/></span>
                                    :
                                    <span><b>Sexo: </b>Masculino<br/></span>
                                }
                                <b>Fecha de nacimiento: </b>{this.props.paciente.fec_nacimiento}<br/>
                                <b>Sede: </b><span>{this.props.paciente.sede}</span><br/>
                                <b>Provincia: </b><span>{this.props.paciente.provincia}</span><br/>
                                <b>Dirección: </b>{this.props.paciente.direccion}<br/>
                                <b>Correo: </b>{this.props.paciente.correo}<br/>
                                <b>Estado civil: </b>{this.props.paciente.estado_civil}
                                <hr/>
                                <b>Estado del Estudiante: </b>{this.props.paciente.estado_estudiante}
                            </p>
                        </div>
                      </ModalBody>
                    </Modal> 
                    {this.props.hc ?
                      <Popover style={{fontSize: '15px'}} placement="bottom" isOpen={this.state.popoverOpen} target="Popover1" toggle={this.toggle}>
                          <PopoverHeader style={{backgroundColor: '#005073', color: '#fff', textTransform: 'capitalize'}}>Diagnóstico</PopoverHeader>
                          <PopoverBody>
                            {this.props.historialclinico.diagnostico_presuntivo}
                          </PopoverBody>
                          <PopoverHeader style={{backgroundColor: '#005073', color: '#fff', textTransform: 'capitalize'}}>Fecha de Diagnóstico</PopoverHeader>
                          <PopoverBody>
                            {this.props.historialclinico.fecha_primeraconsulta}
                          </PopoverBody>
                      </Popover> :  <Popover style={{fontSize: '15px'}} placement="bottom" isOpen={this.state.popoverOpen} target="Popover1" toggle={this.toggle}>
                                          <PopoverHeader style={{backgroundColor: '#f86c6b', color: '#fff', textTransform: 'capitalize'}}>Atención</PopoverHeader>
                                          <PopoverBody>
                                              <p>
                                                  El paciente aún NO tiene una Historia Clínica
                                              </p>
                                          </PopoverBody>
                                    </Popover>
                      
                    }
                </div> : <NavLink href="#/listapacientes">
                            <span class="navlink-default">Aún no has escogido ningún paciente</span>
                        </NavLink>
            }
          </NavItem>
          
        </Nav>
        <Nav className="ml-auto" navbar>
          {this.props.paciente.dip != 'Indefinido' &&
            <div className="animated fadeIn" class="escoje-usuario">
              {this.props.hc ?
                <NavItem className="px-3" >
                    <i className="cui-clipboard icons font-2xl mt-4" style={{color: '#005073'}}></i>
                </NavItem>
                : <NavItem className="px-3">
                    <i className="cui-ban icons font-2xl mt-4" style={{color: '#f86c6b'}}></i>
                  </NavItem>
              }
            </div>
          }
          <AppHeaderDropdown direction="down">
            <DropdownToggle nav>
              <img src={'assets/img/avatars/6.jpg'} className="img-avatar" alt="admin@bootstrapmaster.com" />
            </DropdownToggle>
            <DropdownMenu right style={{ right: 'auto' }}>
              <DropdownItem header tag="div" className="text-center"><strong>Account</strong></DropdownItem>
              <DropdownItem><i className="fa fa-bell-o"></i> Updates<Badge color="info">42</Badge></DropdownItem>
              <DropdownItem><i className="fa fa-envelope-o"></i> Messages<Badge color="success">42</Badge></DropdownItem>
              <DropdownItem><i className="fa fa-tasks"></i> Tasks<Badge color="danger">42</Badge></DropdownItem>
              <DropdownItem><i className="fa fa-comments"></i> Comments<Badge color="warning">42</Badge></DropdownItem>
              <DropdownItem header tag="div" className="text-center"><strong>Settings</strong></DropdownItem>
              <DropdownItem><i className="fa fa-user"></i> Profile</DropdownItem>
              <DropdownItem><i className="fa fa-wrench"></i> Settings</DropdownItem>
              <DropdownItem><i className="fa fa-usd"></i> Payments<Badge color="secondary">42</Badge></DropdownItem>
              <DropdownItem><i className="fa fa-file"></i> Projects<Badge color="primary">42</Badge></DropdownItem>
              <DropdownItem divider />
              <DropdownItem><i className="fa fa-shield"></i> Lock Account</DropdownItem>
              <DropdownItem><i className="fa fa-lock"></i> Logout</DropdownItem>
            </DropdownMenu>
          </AppHeaderDropdown>
        </Nav>
        <AppAsideToggler className="d-md-down-none" />
        {/*<AppAsideToggler className="d-lg-none" mobile />*/}
      </React.Fragment>
    );
  }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

export default DefaultHeader;
