# SI-GESHO-PROMES build environment
FROM node:8.11.1 as builder


LABEL bo.umsa.promes.version="0.0.1-beta"
LABEL vendor="PROMES"
LABEL com.example.release-date="2018-12-05"

RUN mkdir /usr/src/app
WORKDIR /usr/src/app
# ENV PATH /usr/src/app/node_modules/.bin:$PATH
#COPY package.json /usr/src/app/package.json
COPY . /usr/src/app
RUN npm install --silent
RUN npm install yarn serve -g
RUN yarn build
RUN yarn global add serve

EXPOSE 5000
CMD ["serve","-s","build"]